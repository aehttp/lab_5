/*file name: kuts_lab_5_3 
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 30.11.2021
*дата останньої зміни 30.11.2021
*Лабораторна №5
*завдання :Розробити блок-схему алгоритму та
реалізувати його мовою С\С++ відповідно варіанту індивідуального завдання, що
наведено у таблиці Ж.1 з додатку Ж
*призначення програмного файлу:  підрахунок елементів нескінченного ряду.
*варіант : №25
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
using namespace std;
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int n=0, i = 1000;
  double e=pow(10,-3), sum=0, a, b;
  n=1;
  a=fabs((n+2)/n*n);
  n=0;
  do
  {
    i--;
    n++;
    b=pow((-1),n)*(n+2);
    sum=sum+b;
  }
  while(a<=e && i != 0);
  printf("сума= %10.7f n= %d\n",sum,n);
	system("pause");
	return 0;
}

