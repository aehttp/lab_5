/*file name: kuts_lab_5_2 
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 30.11.2021
*дата останньої зміни 30.11.2021
*Лабораторна №5
*завдання :Розробити блок-схему алгоритму для табулювання функції та
реалізувати його мовою С\С++ відповідно варіанту індивідуального завдання, що
наведено у таблиці Е.1 з додатку Е
*призначення програмного файлу: табулювання заданої функції
*варіант : №25
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
using namespace std;
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	double x, y, xstart, xend, xstep;
	
	cout<<" введіть початкое значення x = \t";
	cin>>xstart;
	cout<<" введіть кінцеве значення x = \t";
	cin>>xend;
	cout<<" введіть значення зміни x = \t";
	cin>>xstep;
	printf("_______________________ \n");
	printf("|     х    |    у     | \n");
	x=xstart;
		do
		{
			if (x>=0.5)
			{
				y=pow(3*x+x*x, 1/4);
			}
			else
			{
				y=sin(x*x)+5;
			}
		printf("-----------------------\n");	
		printf("| %8.2f | %8.4f |\n", x, y);
		x=x+xstep;
		}
		while (x<=xend);
		printf("-----------------------\n");
	system("pause");
	return 0;
}

