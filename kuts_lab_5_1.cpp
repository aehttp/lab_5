/*file name:kuts_lab_5_1
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 30.11.2021
*дата останньої зміни 30.11.2021
*Лабораторна №5
*завдання :Розробити блок-схему алгоритму та реалізувати його мовою С\С++ для підрахунку сум та добутків, відповідно індивідуального завдання
*призначення програмного файлу: Розрахунок значення за заданою формулою
*варіант : №4
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
using namespace std;
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	double sum, mul, P;
	int i,x, k, a=2.51, i_start, i_end, k_start, k_end;
	cout<<" введіть початкові значення i =\t";
	cin>>i_start;
	cout<<" введіть кінцеве значення i =  \t";
	cin>>i_end;
	cout<<" введіть початкові значення k =\t";
	cin>>k_start;
	cout<<" введіть кінцеве значення k =  \t";
	cin>>k_end;
	cout<<" введіть х =                   \t";
	cin>>x;
	sum=0;
	k=k_start-1;
		do
		{
		k++;
		sum+=k*sin(k);
		}
		while (k<=k_end);
	mul=1;
	i=i_start-1;
		do
		{
		i++;
			mul*=pow(sin(x*i),2)+2*exp(-x+i);
		}
		while (i<=i_end);
	P=sqrt(5.32*a)*sum+mul;
	cout<<" сума = \t"<< sum <<endl;
	cout<<" добуток = \t"<< mul <<endl;
	printf(" P = %7.5f   \n   ", P);
	system("pause");
	return 0;
}

